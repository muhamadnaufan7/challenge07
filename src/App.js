import { Route, Routes } from "react-router-dom";
import HeaderNav from "./components/Header_Nav";
import Cars from "./pages/Cars";
import Index from "./pages/Index";

function App() {
  return (
    <div className="App">
      <HeaderNav />

      <Routes>
        <Route path="/" element={<Index />} />
        <Route path="/cars" element={<Cars />} />
      </Routes>
    </div>
  );
}

export default App;
