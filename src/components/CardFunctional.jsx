import React, { useEffect } from 'react';
import user from '../assets/images/fi_users.png'
import setting from '../assets/images/fi_settings.png'
import calendar from '../assets/images/fi_calendar.png'
import '../assets/css/index.css'
import { useDispatch, useSelector } from 'react-redux';
import { getCars } from '../store/actions';

const CardFunctional = () => {

    const dispatch = useDispatch()
    const data = useSelector(state => state.car.data)

    useEffect(() => {
        dispatch(getCars({ number: '', date: '' }))
    }, []);

    return (
        <div>
                <div class="container">
                    <div class="row">
                    {data.map((item) =>
                        <div class="col-lg-4">
                            <div class="card mt-5">
                            <img src={item.image.replace("./images", "https://res.cloudinary.com/drxtnctsm/image/upload/v1653569553/challenge-7/")} class="card-img-top img-car" alt="..." />
                            <div class="card-body">
                                <p><b>{item.manufacture} {item.model}</b></p>
                                <h5 class="card-title"><b>Rp. {item.rentPerDay} / hari</b></h5>
                                <p class="card-text">{item.description}</p>
                                <div class="d-flex flex-row">
                                    <img src={user} alt="" width={20} height={20} /><p class="ps-2">{item.capacity} orang</p>
                                </div>
                                <div class="d-flex flex-row">
                                    <img src={setting} alt="" width={20} height={20} /><p class="ps-2">{item.transmission}</p>
                                </div>
                                <div class="d-flex flex-row">
                                    <img src={calendar} alt="" width={20} height={20} /><p class="ps-2">{item.year}</p>
                                </div>
                                <button class="btn btn-primary button-select-car">Pilih Mobil</button>
                            </div>
                            </div>
                        </div>
                    
                )}
                    </div>
                </div>
                
            </div>
    );
}

export default CardFunctional;
