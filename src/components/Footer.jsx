import React, { Component } from 'react';
import '../assets/css/index.css';
import imgFB from '../assets/images/icon_facebook.png';
import imgIG from '../assets/images/icon_instagram.png';
import imgMail from '../assets/images/icon_mail.png';
import imgTW from '../assets/images/icon_twitter.png';
import imgTwitch from '../assets/images/icon_twitch.png'
import logo from '../assets/images/logo.jpg'
import Nav from './Nav';

class Footer extends Component {
    render() {
        return (
            <div>
                {/* Footer */}
                <div className="container footer">
                    <div className="row">

                        <div className="col-lg-4 alamat">
                            <p>Jalan Suroyo No.161 Mayangan Kota<br />
                            Probolinggo 672000</p>
                            <p>binarcarrental@gmail.com</p>
                            <p>081-233-334-808</p>
                        </div>

                        <div className="col-lg-2 ">
                            <nav className="area-navbar-footer">
                                <Nav />
                            </nav>
                        </div>

                        <div className="col-lg-4 sosmed">
                            <p>Connect with us</p>
                            <a href="#" className='px-1'><img src={imgFB} alt="facebook" /></a>
                            <a href="#" className='px-1'><img src={imgIG} alt="instagram" /></a>
                            <a href="#" className='px-1'><img src={imgMail} alt="instagram" /></a>
                            <a href="#" className='px-1'><img src={imgTW} alt="twitter" /></a>
                            <a href="#" className='px-1'><img src={imgTwitch} alt="twitch" /></a>
                        </div>

                        <div className="col-lg-2 copyright">
                            <p>Copyright Binar 2022</p>
                            <img src={logo} alt="logo" />
                        </div>
                    </div>
                </div>
                {/* End of Footer */}
            </div>
        );
    }
}

export default Footer;
