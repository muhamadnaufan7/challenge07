import React, { Component } from 'react';
import { Link } from 'react-router-dom';

class Nav extends Component {
    render() {
        return (
            <div>
                    <li><a href='#our_services'>Our Services</a></li>
                    <li><a href='#why_us'>Why Us</a></li>
                    <li><a href='#testimonial'>Testimonial</a></li>
                    <li><a href='#faq'>FAQ</a></li>
                    <button className='p-1'><a href='#'>Register</a></button>
            </div>
        );
    }
}

export default Nav;
