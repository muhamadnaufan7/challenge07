import React, { Component } from 'react';
import logo from '../assets/images/logo.jpg';
import imgCar from '../assets/images/img_car.png';
import '../assets/css/index.css'
import Nav from './Nav';

class HeaderNav extends Component {
    render() {
        return (
            <div>

                {/* Header */}
                <div className="container-fluid">
                    <div className="row header py-2">

                        {/* Logo */}
                        <div className="col-lg-2">
                            <img src={logo} />
                        </div>
                        {/* End of Logo */}

                        {/* Nav bar */}
                        <div className="col-lg-10 d-flex justify-content-end align-content-center">
                            <nav className='navbar-header'>
                                <Nav />
                            </nav>
                        </div>

                    </div>
                </div>
                {/* End of Header */}

            </div>
        );
    }
}

export default HeaderNav;
