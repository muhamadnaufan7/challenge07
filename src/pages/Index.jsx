import React, { Component } from 'react';
import checklist from '../assets/images/Group53.png';
import girl from '../assets/images/img_service.png';
import iComplete from '../assets/images/icon_complete.png';
import iPrice from '../assets/images/icon_price.png';
import iOneDay from '../assets/images/icon_24hrs.png';
import iProfessional from '../assets/images/icon_professional.png';
import cWoman from '../assets/images/img_photo.png';
import cMan from '../assets/images/img_photo_w.png';
import rate from '../assets/images/Rate.png';
import Footer from '../components/Footer';
import imgCar from '../assets/images/img_car.png';
import { Link } from 'react-router-dom';

class Index extends Component {
    constructor(props) {
        super(props);
        
        this.state = {
            description : "Sewa Mobil Dengan Supir di Bekasi 12 Jam"
        }
    }
    

    render() {
        return (
            <div>
                {/* Top Main */}
                <div className="container-fluid hero">
                    <div className="row">

                    
                    <div className="col-lg-5 area-heading">
                    <h3><b>Sewa & Rental Mobil Terbaik di<br />
                        Kawasan Bekasi
                    </b></h3>
                    <p>Selamat datang di Binar Car Rental. Kami menyediakan mobil kualitas<br />
                    terbaik dengan harga terjangkau. Selalu siap melayani kebutuhanmu<br />
                    untuk sewa mobil selama 24 jam.</p>
                    <button className='button-rent-car p-1'><Link to={'/cars'}>Mulai Sewa Mobil</Link></button>
                    </div>

                    
                    <div className="col-lg-7 area-img-car d-flex justify-content-end">
                    <img src={imgCar} />
                    </div>
                    </div>
                </div>
                {/* End of top main */}
                {/* Girl */}
                <div id="our_services" class="container pt-5">
                    <div class="row">

                    
                    <div class="col-xl-6 col-lg-6 col-md-6">
                        <img class="img-fluid img-girl" src={girl} />
                    </div>
                    
                    
                    <div class="col-xl-6 col-lg-6 col-md-6 teks-services">
                    <h4><b>Best Car Rental for any kind of trip in<br />
                        Bekasi!
                    </b></h4>
                    <p>Sewa mobil di Bekasi bersama Binar Car Rental Jaminan harga lebih<br />
                    murah dibandingkan yang lain, kondisi mobil baru, serta kualitas<br />
                    pelayanan terbaik untuk perjalanan wisata, bisnis, wedding, meeting, dll.</p>
                    <p><img src={checklist} class="ceklis pe-1"  />{this.state.description}</p>
                    <p><img src={checklist} class="ceklis pe-1"  />Sewa Mobil Lepas Kunci di Bekasi 24 Jam</p>
                    <p><img src={checklist} class="ceklis pe-1"  />Sewa Mobil Jangka Panjang Bulanan</p>
                    <p><img src={checklist} class="ceklis pe-1"  />Gratis Antar - Jemput Mobil di Bandara</p>
                    <p><img src={checklist} class="ceklis pe-1"  />Layanan Airport Transfer / Drop in Out</p>
                    </div>
                    </div>
                </div>
                {/* End of girl */}

                {/* Why us */}
                <div id="why_us" class="teks-why-us ps-1 pt-5">
                    <h4><b>Why Us?</b></h4>
                    <p>Mengapa harus pilih Binar Car Rental?</p>
                </div>

                <div class="container-fluid kelebihan">
                    <div class="item">
                        <img src={iComplete} /><br /><br />
                            <h5><b>Mobil Lengkap</b></h5><br />
                            <p>Tersedia banyak pilihan mobil,<br />
                            Kondisi masih baru, bersih dan<br />
                            terawat</p>
                    </div>
                    
                    <div class="item">
                        <img src={iPrice} /><br /><br />
                            <h5><b>Harga Murah</b></h5><br />
                            <p>Harga murah dan bersaing, bisa<br />
                            bandingkan harga kami dengan<br />
                            rental mobil lain</p>
                    </div>
                    
                    <div class="item">
                        <img src={iOneDay}/><br /><br />
                        <h5><b>Layanan 24 Jam</b></h5><br />
                        <p>Siap melayani kebutuhan Anda<br />
                        selama 24 jam nonstop. Kami juga tersedia di akhir minggu</p>
                    </div>
                    
                    <div class="item">
                        <img src={iProfessional} /><br /><br />
                        <h5><b>Sopir Profesional</b></h5><br />
                        <p>Sopir yang profesional,<br />
                        berpengalaman, jujur, ramah dan<br />
                        selalu tepat waktu</p>
                    </div>
                </div>
                {/* End of why us */}

                {/* Testimonial */}
                <div id="testimonial" class="d-flex flex-column align-items-center pt-5">
                    <h4><b>Testimonial</b></h4>
                    <p>berbagai review positif dari para pelanggan kami</p><br />
                </div>
                {/* End of testimonial */}

                {/* Carousel */}
                <div class="container pt-2">
                                <div id="carouselExampleControls" class="carousel slide" data-bs-ride="carousel">
                                <div class="carousel-inner">
                                <div class="carousel-item active">
                                    <div class="container crsl-img">
                                        
                                            <div class="col-xl-1 col-lg-1 col-4 person-photo">
                                                <img class="img-fluid" src={cWoman} />
                                            </div>
                                            <div class="col-xl-11 col-lg-11 col-8">
                                                <img class="rate" src={rate} />
                                                <p>
                                                    “Lorem ipsum dolor sit amet, consectetur adipiscing
                                                    elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur
                                                    adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet,
                                                    consectetur adipiscing elit, sed do eiusmod”
                                                </p>
                                                <p>
                                                    <b>John Dee 32, Bromo</b>
                                                </p>
                        
                                            </div>
                                        
                                    </div>
                                </div>
                                <div class="carousel-item">
                                    <div class="container crsl-img">
                                        
                                            <div class="col-xl-1 col-lg-1 col-4 person-photo">
                                            <img class="img-fluid" src={cMan} />
                                            </div>
                                            <div class="col-xl-11 col-lg-11 col-8">
                                                <img class="rate" src={rate} />
                                                <p>
                                                    “Lorem ipsum dolor sit amet, consectetur adipiscing
                                                    elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur
                                                    adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet,
                                                    consectetur adipiscing elit, sed do eiusmod”
                                                </p>
                                                <p>
                                                    <b>John Dee 32, Bromo</b>
                                                </p>
                        
                                            </div>
                            
                                    </div>
                                </div>
                                <div class="carousel-item">
                                    <div class="container crsl-img">
                                        
                                            <div class="col-xl-1 col-lg-1 col-4 person-photo">
                                                <img class="img-fluid" src={cWoman} />
                                            </div>
                                            <div class="col-xl-11 col-lg-11 col-8">
                                                <img class="rate" src={rate} />
                                                <p>
                                                    “Lorem ipsum dolor sit amet, consectetur adipiscing
                                                    elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur
                                                    adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet,
                                                    consectetur adipiscing elit, sed do eiusmod”
                                                </p>
                                                <p>
                                                    <b>John Dee 32, Bromo</b>
                                                </p>
                        
                                            </div>
                                    
                                    </div>
                                </div>
                                </div>

                            <div class="btn-carousel pt-5">
                                <button class="carousel-control " type="button" data-bs-target="#carouselExampleControls"
                                    data-bs-slide="prev">
                                    <span class="carousel-control-prev-icon tombol-crsl" aria-hidden="true"></span>
                                    <span class="visually-hidden prev">Previous</span>
                                </button>
                                <button class="carousel-control" type="button" data-bs-target="#carouselExampleControls"
                                    data-bs-slide="next">
                                    <span class="carousel-control-next-icon tombol-crsl" aria-hidden="true"></span>
                                    <span class="visually-hidden next">Next</span>
                                </button>
                            </div>
                            
                        <br />
                    </div>
                </div>
                {/* End of carousel */}

                {/* Sewa mobil */}
                <div class="container con-rent pt-5 px-3">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-12 sewa-mobil">
                        <h2><b>Sewa Mobil di Bekasi Sekarang</b></h2>
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Rerum accusamus repellendus,<br /> 
                            tempore mollitia quae rem culpa odit dolorum ipsam.</p>
                        <br /><br />
                        <button>Mulai Sewa Mobil</button>
                    </div>
                </div>
                {/* End of sewa mobil */}

                {/* FAQ */}
                <div id="faq" class="container pt-5 px-2 d-flex flex-row"> 
                
                        <div class="col-xl-6 col-lg-6 col-md-6 faq p-0">
                            <h4><b>Frequently Asked Question</b></h4>
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit</p>
                        </div>
            
                        <div class="col-xl-6 col-lg-6 col-md-6 accordion faq-accordion p-0" id="accordionPanelsStayOpenExample">
                                <div class="accordion-item">
                                <h2 class="accordion-header" id="panelsStayOpen-headingOne">
                                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#panelsStayOpen-collapseOne" aria-expanded="true" aria-controls="panelsStayOpen-collapseOne">
                                    Apa saja syarat yang dibutuhkan?
                                    </button>
                                </h2>
                                <div id="panelsStayOpen-collapseOne" class="accordion-collapse collapse" aria-labelledby="panelsStayOpen-headingOne">
                                    <div class="accordion-body">
                                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Aperiam in accusantium suscipit eos officia, quia adipisci asperiores nam perferendis recusandae unde fugiat culpa ut id excepturi maxime nostrum. Quod, quos.</div>
                                </div>
                                </div>
                                <div class="accordion-item">
                                <h2 class="accordion-header" id="panelsStayOpen-headingTwo">
                                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#panelsStayOpen-collapseTwo" aria-expanded="false" aria-controls="panelsStayOpen-collapseTwo">
                                    Berapa hari minimal sewa mobil lepas kunci?
                                    </button>
                                </h2>
                                <div id="panelsStayOpen-collapseTwo" class="accordion-collapse collapse" aria-labelledby="panelsStayOpen-headingTwo">
                                    <div class="accordion-body">
                                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Repellendus accusantium placeat iste quasi recusandae nulla magnam deleniti error, minus rem libero inventore esse vero unde qui numquam hic voluptatibus debitis?</div>
                                </div>
                                </div>
                                <div class="accordion-item">
                                <h2 class="accordion-header" id="panelsStayOpen-headingThree">
                                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#panelsStayOpen-collapseThree" aria-expanded="false" aria-controls="panelsStayOpen-collapseThree">
                                    Berapa hari sebelumnya sebaiknya booking sewa mobil?
                                    </button>
                                </h2>
                                <div id="panelsStayOpen-collapseThree" class="accordion-collapse collapse" aria-labelledby="panelsStayOpen-headingThree">
                                    <div class="accordion-body">
                                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Praesentium, cupiditate. Voluptate at voluptas optio itaque adipisci vel, voluptatem, reiciendis quibusdam aut rerum, nesciunt dolorem aspernatur esse! Ratione est obcaecati officia!</div>
                                </div>
                                </div>
                                <div class="accordion-item">
                                    <h2 class="accordion-header" id="panelsStayOpen-headingFour">
                                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#panelsStayOpen-collapseFour" aria-expanded="false" aria-controls="panelsStayOpen-collapseFour">
                                    Apakah ada biaya antar-jemput?
                                    </button>
                                    </h2>
                                    <div id="panelsStayOpen-collapseFour" class="accordion-collapse collapse" aria-labelledby="panelsStayOpen-headingFour">
                                    <div class="accordion-body">
                                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Praesentium, cupiditate. Voluptate at voluptas optio itaque adipisci vel, voluptatem, reiciendis quibusdam aut rerum, nesciunt dolorem aspernatur esse! Ratione est obcaecati officia!</div>
                                    </div>
                                </div>
                                <div class="accordion-item">
                                    <h2 class="accordion-header" id="panelsStayOpen-headingFive">
                                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#panelsStayOpen-collapseFive" aria-expanded="false" aria-controls="panelsStayOpen-collapseFive">
                                        Bagaimana jika terjadi kecelakaan?
                                    </button>
                                    </h2>
                                    <div id="panelsStayOpen-collapseFive" class="accordion-collapse collapse" aria-labelledby="panelsStayOpen-headingFive">
                                    <div class="accordion-body">
                                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Praesentium, cupiditate. Voluptate at voluptas optio itaque adipisci vel, voluptatem, reiciendis quibusdam aut rerum, nesciunt dolorem aspernatur esse! Ratione est obcaecati officia!</div>
                                    </div>
                                </div>
                        </div>
                </div>
                {/* End of FAQ */}

                {/* Footer */}
                <Footer />
                {/* End of footer */}
                {/* End of main */}
            </div>
        );
    }
}

export default Index;
