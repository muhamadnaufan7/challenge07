import React, { Component } from 'react';
import Footer from '../components/Footer';
import CardFunctional from '../components/CardFunctional';
import { connect } from 'react-redux';
import { getCars } from '../store/actions';
import imgCar from '../assets/images/img_car.png';


class Cars extends Component {
    constructor(props) {
        super(props);
        
        this.state = {
            paragraphCtn : 'Tipe Driver',
            date : '',
            number : ''
        }
    }
    
    handleChangeDate = (e) => {
        this.setState({
            date : e.target.value
        })
    }

    handleChangeJumlahPenumpang = (e) => {
        this.setState({
            number : e.target.value
        })
    }

    handleSubmit = (e) => {
        let objMobil = {
            number : this.state.number,
            date : this.state.date
        }

        console.log(objMobil)
        this.props.getCars(objMobil)
    }

    render() {
        return (
            <div>
                {/* Top Main */}
                <div className="container-fluid hero">
                    <div className="row">

                    
                    <div className="col-lg-5 area-heading">
                    <h3><b>Sewa & Rental Mobil Terbaik di<br />
                        Kawasan Bekasi
                    </b></h3>
                    <p>Selamat datang di Binar Car Rental. Kami menyediakan mobil kualitas<br />
                    terbaik dengan harga terjangkau. Selalu siap melayani kebutuhanmu<br />
                    untuk sewa mobil selama 24 jam.</p>
                    </div>

                    
                    <div className="col-lg-7 area-img-car d-flex justify-content-end">
                    <img src={imgCar} />
                    </div>
                    </div>
                </div>
                {/* End of top main */}

                {/* Form filter */}
                <div className='mb-5'>
                <div className="container filter-sewa">
                    <div className="row justify-content-md-evenly">
                    
                        <div className="col col-lg-2">
                            
                            <p>{this.state.paragraphCtn}</p>
                            <div>
                            <select name="driver" id="tipe-driver">
                                <option value="1">Dengan Sopir</option>
                                <option value="2">Tanpa Sopir (Lepas Kunci)</option>
                            </select>
                            </div>
                        </div>
                        <div className="col col-lg-2">
                            <p>Tanggal</p>
                            <div>
                                    <input id="date-sewa" type="date" onChange={(e) => this.handleChangeDate(e)} />
                            </div>
                        </div>
                        <div className="col col-lg-2">
                            <p>Waktu Jemput/Ambil</p>
                            <div>
                                
                                <select name="jam" id="waktu-jemput">
                                <option value="1">08.00 &emsp;&emsp; WIB</option>
                                <option value="2">09.00 &emsp;&emsp; WIB</option>
                                <option value="3">10.00 &emsp;&emsp; WIB</option>
                                <option value="4">11.00 &emsp;&emsp; WIB</option>
                                <option value="5">12.00 &emsp;&emsp; WIB</option>
                                </select>
                            </div>
                        </div>
                        <div className="col col-lg-2">
                            <p>Jumlah Penumpang (Optional)</p>
                            <div className="input-group mb-3 input-group-sm">
                                <input id="jumlah-penumpang" type="number" onChange={(e) => this.handleChangeJumlahPenumpang(e)} />
                        </div>
                        </div>
                        <div className="col-md-auto button-cari-mobil">
                            <button id="cari-mobil" onClick={(e) => this.handleSubmit(e)}><b>Cari Mobil</b></button>
                        </div>
                        
                    </div>
                </div>
                </div>
                {/* End of form filter */}

                {/* Cars card */}
                <CardFunctional />
                {/* End of cars card */}

                {/* Footer */}
                <Footer />
                {/* End of footer */}
                {/* End of Main Cars */} 
            </div>
        );
    }
}

export default connect(null, {getCars}) (Cars);
