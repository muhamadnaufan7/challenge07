// import { createStore } from 'redux'
// import reducers from './reducers'

// const store = createStore(reducers)

// export default store

// import redux middleware
import { applyMiddleware, createStore } from 'redux'
// import reduc
import reducers from './reducers'
// import thunk
import thunk from 'redux-thunk'

const store = createStore(
    reducers,
    applyMiddleware(thunk)
)

export default store
