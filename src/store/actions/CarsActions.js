import axios from "axios";
import moment from "moment";

export const GET_CARS = "GET_CARS";

export const getCars = ({ number, date }) => async dispatch => {
    try {
        const res = await axios.get('https://raw.githubusercontent.com/fnurhidayat/probable-garbanzo/main/data/cars.min.json');
        console.log({ number, date: moment(date).format('YYYY-MM-DD') });
        
        //mengirim data ke card
        dispatch({
            type: GET_CARS,
            payload: res.data
        })

        //kondisi ketika filter berdasarkan tanggal sewa & jumlah penumpang
        if (date !== "" && number !== "" ) {
            const data = res.data.filter(item => {
                return moment(item.availableAt).format('YYYY-MM-DD') <= moment(date).format('YYYY-MM-DD') && parseInt(item.capacity) <= parseInt(number);
            });
            dispatch({
                type: GET_CARS,
                payload: data
            })
        } else {
            dispatch({
                type: GET_CARS,
                payload: res.data
            })
        }

        // kondisi ketika filter berdasarkan tanggal sewa saja 
        if (date !== "") {
            const data = res.data.filter(item => {
                return moment(item.availableAt).format('YYYY-MM-DD') <= moment(date).format('YYYY-MM-DD');
            });
            dispatch({
                type: GET_CARS,
                payload: data
            })
        } 
        
        // kondisi ketika filter berdasarkan jumlah penumpang saja
        if (number !== "") {
            const data = res.data.filter(item => {
                return parseInt(item.capacity) <= parseInt(number);
            });
            dispatch({
                type: GET_CARS,
                payload: data
            })
        }

    } catch (error) {
        console.log(error)       
    }
}