// import combine reducers
import { combineReducers } from "redux";

// import reducers
import carsReducers from "./CarsReducers";

const reducers = combineReducers({
    car : carsReducers
})

export default reducers