import { GET_CARS } from "../actions";

export default function carsReducers(state = { data : [] }, action) {
    switch (action.type) {
        case GET_CARS:
            return {...state, data: action.payload}

        default:
            return state;
    }
}